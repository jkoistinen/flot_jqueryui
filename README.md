# Flot_jQueryUI

Install with:
virtualenv yourenv;
cd yourenv;
pip install -r requirements.txt;
python flottemphum.py

Dataformat of temprh.log:
26.5° 37% 2015-11-24 21:07:54

# Screenshot
![alt tag](https://cloud.githubusercontent.com/assets/1724431/11385747/0b6b11be-931c-11e5-83f6-b6db8fbd0412.png)
