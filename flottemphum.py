from flask import Flask
from flask import render_template, url_for

import datetime

#Static content. Other parameters can be added here aswell.
app = Flask(__name__,static_folder="/root/scripts/venv/flottemphum/flot")
#app = Flask(__name__)
#test

# Turn this off
app.debug = True

@app.route('/temphum/<year>/<month>/<day>')
def temphum(year, month, day):

    #unixtime in millisecond.
    #var temperature = [[1,1], [20000000,3], [1300000000,4]]
    # [1,1], [20000000,3], [1300000000,4]

    #var humidity = [[1,4],[20000000,7],[20000000,7],[1300000000,14]]

    temp = []
    hum  = []

    datestring = str(year)+'-'+str(month)+'-'+str(day)

    datestring_url = str(year)+'/'+str(month)+'/'+str(day)

    logdata = open('/var/log/temprh.log','r')
    for line in logdata.readlines():
        #line format is [TEMP, HUM, DATE, TIME]
        if datestring in line:
            line = line.split(' ')
            date = line[2].split('-')
            timestamp = line[3].split(':')
            dt = datetime.datetime(int(date[0]), int(date[1]), int(date[2]), int(timestamp[0]), int(timestamp[1]), int(timestamp[2]))
            logged_timestamp = dt.strftime("%s")
            logged_temp = line[0][:-2]
            logged_hum = line[1][:-1]
            print line

            #convert to millisecond
            logged_timestamp = str(int(logged_timestamp) * 1000)

            new_temp = "["+logged_timestamp+","+logged_temp+"]"+","
            new_hum = "["+logged_timestamp+","+logged_hum+"]"+","

            temp.append(new_temp)
            hum.append(new_hum)

    print len(temp)
    print len(hum)

    vartemp = ''.join(temp)
    varhum = ''.join(hum)

    return render_template('index.html', vartemp=vartemp, varhum=varhum, datestring=datestring, datestring_url=datestring_url)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
